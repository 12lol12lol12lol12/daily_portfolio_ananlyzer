from django.contrib import admin

from parsefile.models import (
    AggregateName, NumSc, FirstAnalyzeData, LoadedFile,
    DayBalance, A_P_Aggregator, AggregateBalance,
    AggregateSum)

class AggregateNameAdmin(admin.ModelAdmin):
    list_display = ('number', 'a_p', 'name')

admin.site.register(AggregateName, AggregateNameAdmin)
admin.site.register(NumSc)
admin.site.register(FirstAnalyzeData)
admin.site.register(LoadedFile)
admin.site.register(DayBalance)
admin.site.register(A_P_Aggregator)
admin.site.register(AggregateBalance)
admin.site.register(AggregateSum)
# Register your models here.
