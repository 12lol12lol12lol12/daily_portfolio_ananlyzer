from django.urls import path
from typing import List
from .views import (
    LoadFileView, GetBalanceByDay, AggregateBalanceView,
    GetLastAnalyzeData, GetLastDaysView, GetAllActivesView,
    IsChangedView, GetAllPassivesView
)

app_name = 'parsefile'

urlpatterns: List = [
    path('load_excel', LoadFileView.as_view(), name='load_excel'),
    path('get_balance', GetBalanceByDay.as_view(), name='get_balance'),
    path('aggregate_balance', AggregateBalanceView.as_view()),
    path('last_analyze_data', GetLastAnalyzeData.as_view()),
    path('get_last_dates', GetLastDaysView.as_view()),
    path('get_all_actives', GetAllActivesView.as_view()),
    path('get_all_passives', GetAllPassivesView.as_view()),
    path('is_changed', IsChangedView.as_view()),
]
