from collections import OrderedDict
from exceptions import StoriesException
from datetime import datetime
from django.db.models import Sum
from django.db.transaction import atomic
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from parsefile.models import (A_P_Aggregator, AggregateBalance, AggregateName,
                              AggregateSum, DayBalance, FirstAnalyzeData)
from parsefile.serializers import (AggregateSumSerializer,
                                   DayBalanceSerializer,
                                   FirstAnalyzeDataSerializer,
                                   LoadedFileSerializer)
from parsefile.services import GetAggregateSumStory, ParseExcelFileStory


class LoadFileView(APIView):
    """
    Загрузка файла
    """
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)
    serializer_class = LoadedFileSerializer

    def post(self, request, *args, **kwargs):
        request.data['user'] = request.user.pk
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = serializer.save()
        try:
            story = ParseExcelFileStory().run.run(file=obj, user=self.request.user)
        except StoriesException as ex:
            return Response({'errors': [ex.message]}, status=400)
        if story.is_success:
            return Response(FirstAnalyzeDataSerializer(story.value).data)
        else:
            return Response({'errors': ['Story failed']}, status=400)


class GetBalanceByDay(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        date = request.data['date']
        qs = DayBalance.objects.select_related(
            'num_sc').filter(date=date, user=request.user).order_by('num_sc__bs1')
        return Response(DayBalanceSerializer(qs, many=True).data, status=200)


class AggregateBalanceView(APIView):
    """
    Возваращает агрегированный баланс
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        user = self.request.user
        dates = FirstAnalyzeData.objects.filter(user=user).order_by(
            '-date').values_list('date', flat=True)[:5]
        dates = sorted(dates)
        if not dates:
            return Response({'errors': ['Нету загруженных данных']}, status=400)
        for date in dates:
            if not AggregateBalance.objects.filter(date=date, user=user).exists():
                self.create_balance(date)
        data = {}
        data = OrderedDict()
        for ag in AggregateSum.objects.filter(date__in=dates, user=user).order_by('name__a_p', 'name__number').values('name__name', 'sum', 'date', 'name__number', 'name__id', 'name__a_p'):
            if data.get(ag.get('name__id')):
                data[ag.get('name__id')][str(ag.get('date'))
                                         ] = f"{ag.get('sum'):,}"
            else:
                data[ag.get('name__id')] = {
                    'name': ag.get('name__name'),
                    'number': ag.get('name__number'),
                    'a_p': ag.get('name__a_p'),
                    str(ag.get('date')): f"{ag.get('sum'):,}"
                }
        return Response({
            'headers': dates,
            'data': data.values()
        }, status=200)

    @atomic
    def create_balance(self, date):
        user = self.request.user
        active_aggregator = A_P_Aggregator.objects.create(date=date, a_p='А')
        passive_aggregator = A_P_Aggregator.objects.create(date=date, a_p='П')
        a_balance = AggregateBalance.objects.create(
            user=user,
            date=date,
            active=active_aggregator,
            passive=passive_aggregator
        )
        for a_name in AggregateName.objects.all():
            aggregate_sum = AggregateSum.objects.create(name=a_name,
                                                        a_p_aggregator=active_aggregator if a_name == 'А' else passive_aggregator,
                                                        sum=GetAggregateSumStory().run.run(date=date, name=a_name, user=user).value,
                                                        date=date,
                                                        user=user)
        active_aggregator.sum = AggregateSum.objects.select_related(
            'name').filter(name__a_p='А').aggregate(s=Sum('sum')).get('s')
        active_aggregator.save()
        passive_aggregator.sum = AggregateSum.objects.select_related(
            'name').filter(name__a_p='П').aggregate(s=Sum('sum')).get('s')
        passive_aggregator.save()
        return AggregateSum.objects.select_related('name').filter(date=date, user=user)


class GetLastAnalyzeData(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        f_a = FirstAnalyzeData.objects.filter(user=user).last()
        if not f_a:
            return Response({'errors': ['Вы не загружали данные']}, status=400)
        return Response(FirstAnalyzeDataSerializer(f_a).data, status=200)


class GetLastDaysView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        dates = FirstAnalyzeData.objects.filter(
            user=user).order_by('-date').values('date')
        d = [d.get('date') for d in dates]
        return Response(d, status=200)


class GetAllActivesView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        dates = FirstAnalyzeData.objects.filter(user=user).order_by(
            '-date').values_list('date', flat=True)[:5]
        dates = sorted(dates)
        names = ['Дата']
        data = OrderedDict()
        for ag in AggregateSum.objects.select_related('name').filter(name__a_p='А', user=user, name__is_sum=True).order_by('name__number').values('name__name', 'date', 'sum'):
            if data.get(ag.get('name__name')):
                data[ag.get('name__name')][ag.get('date')] = ag.get('sum')
            else:
                data[ag.get('name__name')] = {
                    ag.get('date'): ag.get('sum')
                }
        sums = [list([i, ]) for i in sorted(list(data.values())[0].keys())]
        for key, values in data.items():
            for i, v in enumerate(sorted(values.keys())):
                sums[i].append(values[v])
        names.extend(data.keys())
        return Response({
            'names': names,
            'data': sums
        })


class GetAllPassivesView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        dates = FirstAnalyzeData.objects.filter(user=user).order_by(
            '-date').values_list('date', flat=True)[:5]
        dates = sorted(dates)
        names = ['Дата']
        data = OrderedDict()
        for ag in AggregateSum.objects.select_related('name').filter(name__a_p='П', user=user, name__is_sum=True).order_by('name__number').values('name__name', 'date', 'sum'):
            if data.get(ag.get('name__name')):
                data[ag.get('name__name')][ag.get('date')] = ag.get('sum')
            else:
                data[ag.get('name__name')] = {
                    ag.get('date'): ag.get('sum')
                }
        sums = [list([i, ]) for i in sorted(list(data.values())[0].keys())]
        for key, values in data.items():
            for i, v in enumerate(sorted(values.keys())):
                sums[i].append(values[v])
        names.extend(data.keys())
        return Response({
            'names': names,
            'data': sums
        })

class IsChangedView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        dates_s = request.data['dates']
        dates = [datetime.strptime(s, '%Y-%m-%d') for s in dates_s]
        if FirstAnalyzeData.objects.filter(user=user).count() != len(dates):
            return Response(status=400)
        return Response('Ok', status=200)
