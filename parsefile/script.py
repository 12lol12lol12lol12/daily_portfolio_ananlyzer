from parsefile.models import NumSc
import pandas as pd


def run():
    xs = pd.ExcelFile('media/b.xlsx')
    df = xs.parse("Баланс")
    ls = []

    for ind in df.index:
        ls.append(
            NumSc(
                cod_1 = df['Код 1'][ind],
                cod_2 = df['Код 2'][ind],
                cod_3 = df['Код 3'][ind],
                cod_4 = df['Код 4'][ind],
                cod_5 = df['Код 5'][ind],
                a_p = df['А / П'][ind],
                bs1 = df['БС1'][ind],
                bs2 = df['БС2'][ind],
                name_bs1=df['Наименование счета первого порядка'][ind],
                name_bs2=df['Наименование счета второго порядка'][ind]
            )
        )
        if ind == 1000:
            NumSc.objects.bulk_create(ls)
            ls = []
            print(ind, end='\r')
    NumSc.objects.bulk_create(ls)

def aggregate_name():
    xs = pd.ExcelFile('media/b.xlsx')
    df = xs.parse("Агрегированный баланс")
    df = df.drop(df.index[0])
    df = df.drop(df.index[0])
    df = df.drop(df.index[0])
    

if __name__ == '__main__':
    run()