# Generated by Django 3.0.3 on 2020-06-09 13:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('parsefile', '0028_auto_20200609_1134'),
    ]

    operations = [
        migrations.AddField(
            model_name='aggregatesum',
            name='user',
            field=models.ForeignKey(default=13, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='firstanalyzedata',
            name='user',
            field=models.ForeignKey(default=13, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
