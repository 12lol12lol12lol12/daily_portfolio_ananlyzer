# Generated by Django 3.0.3 on 2020-05-26 19:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parsefile', '0018_auto_20200526_1852'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aggregatename',
            options={'ordering': ['number']},
        ),
        migrations.RenameField(
            model_name='aggregatesum',
            old_name='childs',
            new_name='parent',
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='min_cod_1',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='min_cod_2',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='min_cod_3',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='min_cod_4',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='min_cod_5',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='min_vpr',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='sum_cod_1',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='sum_cod_2',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='sum_cod_3',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='sum_cod_4',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='sum_cod_5',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
        migrations.AddField(
            model_name='aggregatename',
            name='sum_vpr',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
    ]
