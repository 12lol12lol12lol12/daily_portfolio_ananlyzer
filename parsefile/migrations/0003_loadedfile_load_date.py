# Generated by Django 3.0.3 on 2020-04-15 08:13

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('parsefile', '0002_auto_20200415_0735'),
    ]

    operations = [
        migrations.AddField(
            model_name='loadedfile',
            name='load_date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2020, 4, 15, 8, 13, 43, 769753, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
