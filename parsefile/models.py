from django.db import models
from django.utils.functional import cached_property


class LoadedFile(models.Model):
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    file = models.FileField(upload_to='media/')
    load_date = models.DateTimeField(auto_now_add=True)


class FirstAnalyzeData(models.Model):
    """
    Сумма активов и пассивов в млн руб
    """
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    active_sum = models.BigIntegerField(null=True, blank=True)
    passive_sum = models.BigIntegerField(null=True, blank=True)
    date = models.DateField(unique=True)

    class Meta:
        ordering =['-date']

    @cached_property
    def a_p_diff(self):
        return self.active_sum - self.passive_sum

class NumSc(models.Model):
    """
    Счет и информация о нем
    """
    cod_1 = models.CharField(max_length=10, null=True, blank=True)
    cod_2 = models.CharField(max_length=10, null=True, blank=True)
    cod_3 = models.CharField(max_length=10, null=True, blank=True)
    cod_4 = models.CharField(max_length=10, null=True, blank=True)
    cod_5 = models.CharField(max_length=10, null=True, blank=True)
    a_p = models.CharField(max_length=10) # актив или пассив
    bs1 = models.IntegerField() # номер счета первого порядка
    bs2 = models.IntegerField(primary_key=True) # номер счета второго порядка
    name_bs1 = models.CharField(max_length=256) # наименование счета первого пордяка
    name_bs2 = models.CharField(max_length=256) # наименование счета второго пордяка
    aggregate_name = models.ForeignKey('AggregateName', on_delete=models.CASCADE, null=True, blank=True)


class DayBalance(models.Model):
    class A_P(models.IntegerChoices):
        ACTIVE = 1
        PASSIVE = 2
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    num_sc = models.ForeignKey(to=NumSc, on_delete=models.CASCADE)
    a_p = models.IntegerField(choices=A_P.choices)
    iitg = models.BigIntegerField(verbose_name='IITG в млн руб')
    date = models.DateField()

    class Meta:
        unique_together = ('user', 'num_sc', 'date')


class A_P_Aggregator(models.Model):
    date = models.DateField()
    sum = models.BigIntegerField(null=True, blank=True)
    a_p = models.CharField(max_length=10)


class AggregateBalance(models.Model):
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    date = models.DateField()
    active = models.OneToOneField(A_P_Aggregator, on_delete=models.CASCADE, related_name='aggregate_balance_a')
    passive = models.OneToOneField(A_P_Aggregator, on_delete=models.CASCADE, related_name='aggregate_balance_p')

    class Meta:
        unique_together = ('user', 'date')


class AggregateName(models.Model):
    name = models.CharField(max_length=256)
    a_p = models.CharField(max_length=10)
    number = models.FloatField()
    is_sum = models.BooleanField(default=False)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    sum_cod_1 = models.CharField(max_length=128, null=True, blank=True)
    sum_cod_2 = models.CharField(max_length=128, null=True, blank=True)
    sum_cod_3 = models.CharField(max_length=128, null=True, blank=True)
    sum_cod_4 = models.CharField(max_length=128, null=True, blank=True)
    sum_cod_5 = models.CharField(max_length=128, null=True, blank=True)
    min_cod_1 = models.CharField(max_length=128, null=True, blank=True)
    min_cod_2 = models.CharField(max_length=128, null=True, blank=True)
    min_cod_3 = models.CharField(max_length=128, null=True, blank=True)
    min_cod_4 = models.CharField(max_length=128, null=True, blank=True)
    min_cod_5 = models.CharField(max_length=128, null=True, blank=True)
    sum_bs1 = models.CharField(max_length=128, null=True, blank=True)
    sum_vpr = models.CharField(max_length=128, null=True, blank=True)
    min_vpr = models.CharField(max_length=128, null=True, blank=True)
    min_bs1 = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        ordering = ['a_p', 'number']

    def __str__(self):
        return f'{self.number} {self.name} {self.a_p}'


class AggregateSum(models.Model):
    name = models.ForeignKey(AggregateName, on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    a_p_aggregator = models.ForeignKey(A_P_Aggregator, on_delete=models.CASCADE)
    sum = models.BigIntegerField()
    date = models.DateField()
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)


