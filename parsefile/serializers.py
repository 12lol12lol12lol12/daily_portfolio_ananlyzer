from rest_framework import serializers
from .models import (
    LoadedFile, FirstAnalyzeData, DayBalance, NumSc,
    AggregateName, AggregateSum
)


class LoadedFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = LoadedFile
        fields = '__all__'

class FirstAnalyzeDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = FirstAnalyzeData
        fields =('active_sum', 'passive_sum', 'a_p_diff', 'date')

class NumScRelatedSerializer(serializers.ModelSerializer):
    class Meta:
        model = NumSc
        fields = ('bs1', 'bs2', 'name_bs1', 'name_bs2')

class DayBalanceSerializer(serializers.ModelSerializer):
    num_sc = NumScRelatedSerializer(read_only=True)
    class Meta:
        model = DayBalance
        fields = (
            'num_sc', 'a_p', 'iitg', 
        )


class AggregateNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = AggregateName
        fields = (
            'name', 'a_p', 'number'
        )

class AggregateSumSerializer(serializers.ModelSerializer):
    name = AggregateNameSerializer(read_only=True)
    class Meta:
        model = AggregateSum
        fields = (
            'name', 'sum', 'date'
        )