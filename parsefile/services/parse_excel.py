from stories import story, Success, Failure, arguments, Result
from dataclasses import dataclass
import pandas as pd
from parsefile.models import LoadedFile, FirstAnalyzeData, DayBalance, NumSc
from django.db.models import Sum
from django.db.utils import IntegrityError
from exceptions import StoriesException


class ParseExcelFileStory:

    @story
    @arguments('file', 'user')
    def run(I):
        I.read_file
        I.create_day_balance
        I.get_a_p_summ
        I.show_result

    def read_file(self, ctx):
        data = pd.read_excel(ctx.file.file.path)
        ctx.data = data
        return Success()

    def create_day_balance(self, ctx):
        df = ctx.data
        date = df['DT'][1].date()
        ls = []
        nums_sc_ls = {}
        for ind in ctx.data.index:
            num_sc = int(df['NUM_SC'][ind])
            ls.append(
                DayBalance(
                    user=ctx.user,
                    num_sc_id=num_sc,
                    a_p=df['A_P'][ind],
                    date=df['DT'][ind].date(),
                    iitg=df['IITG'][ind]
                )
            )
            nums_sc_ls[num_sc]=df['A_P'][ind]
        num_sc_pks = set(NumSc.objects.values_list('pk', flat=True))
        different = set(nums_sc_ls.keys()) - num_sc_pks
        NumSc.objects.bulk_create([
            NumSc(
                a_p=nums_sc_ls[d],
                bs1=int(str(d)[:3]),
                bs2=d,
                name_bs1='Undefined',
                name_bs2='Undefined'
            ) for d  in different
        ])
        try:
            DayBalance.objects.bulk_create(ls)
        except IntegrityError:
            raise StoriesException('Баланс на этот день уже существует')
        ctx.date = date
        return Success()

    def get_a_p_summ(self, ctx):
        """
        Вычислить сумму пассивных и активных счетов (млн. руб)
        """
        qs = DayBalance.objects.filter(date=ctx.date)
        analyze_data = FirstAnalyzeData.objects.create(
            active_sum=DayBalance.objects.filter(date=ctx.date, a_p=1).aggregate(sum=Sum('iitg')).get('sum'),
            passive_sum=DayBalance.objects.filter(date=ctx.date, a_p=2).aggregate(sum=Sum('iitg')).get('sum'),
            date=ctx.date, user=ctx.user
        )
        ctx.analyze_data = analyze_data
        return Success()

    def show_result(self, ctx):
        """
        Возврат данных
        """
        return Result(
            ctx.analyze_data
        )
