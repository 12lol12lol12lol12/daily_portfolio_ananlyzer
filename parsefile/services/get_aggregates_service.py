from stories import story, Success, Failure, arguments, Result
from dataclasses import dataclass
from django.db.models import Sum
from parsefile.models import DayBalance, NumSc, AggregateName, AggregateSum
from django.db.utils import IntegrityError
from exceptions import StoriesException


class GetAggregateSumStory:
    sum = 0
    min = 0
    sum_vpr = 0
    min_vpr = 0
    sum_bs = 0
    min_bs = 0
    qs = None

    @story
    @arguments('date', 'name', 'user')
    def run(I):
        I.get_qs
        I.get_sum_cod_1
        I.get_sum_cod_2
        I.get_sum_cod_3
        I.get_sum_cod_4
        I.get_sum_cod_5
        I.get_sum_bs1
        I.get_min_cod_1
        I.get_min_cod_2
        I.get_min_cod_3
        I.get_min_cod_4
        I.get_min_cod_5
        I.get_min_bs1
        I.get_sum_vpr
        I.get_min_vpr
        I.show_result

    def parse_cod_field(self, s):
        ls = s.split(';')
        res = []
        for i in ls:
            code = i.split('|')[0]
            a_p = i.split('|')[1] if len(i.split('|')) > 1 else None
            if a_p == 'A':
                a_p = 'А'
            res.append({
                'code': code,
                'a_p': a_p
            })
        return res

    def parse_vpr(self, s):
        ls = s.split(';')
        res = []
        for i in ls:
            vals = i.split(' ')
            res.append({
                'bs2_1': int(vals[0]),
                'sign': vals[1],
                'bs2_2': int(vals[2])
            })
        return res


    def get_qs(self, ctx):
        self.qs = DayBalance.objects.filter(user=ctx.user, date=ctx.date).select_related('num_sc')
        return Success()
    
    def get_sum_cod_1(self, ctx):
        if ctx.name.sum_cod_1:
            res = self.parse_cod_field(ctx.name.sum_cod_1)
            for i in res:
                if i.get('a_p'):
                    self.sum += self.qs.filter(num_sc__cod_1=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.sum += self.qs.filter(num_sc__cod_1=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()
    

    def get_sum_cod_2(self, ctx):
        if ctx.name.sum_cod_2:
            res = self.parse_cod_field(ctx.name.sum_cod_2)
            for i in res:
                if i.get('a_p'):
                    self.sum += self.qs.filter(num_sc__cod_2=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.sum += self.qs.filter(num_sc__cod_2=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()


    def get_sum_cod_3(self, ctx):
        if ctx.name.sum_cod_3:
            res = self.parse_cod_field(ctx.name.sum_cod_3)
            for i in res:
                if i.get('a_p'):
                    self.sum += self.qs.filter(num_sc__cod_3=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.sum += self.qs.filter(num_sc__cod_3=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

    def get_sum_cod_4(self, ctx):
        if ctx.name.sum_cod_4:
            res = self.parse_cod_field(ctx.name.sum_cod_4)
            for i in res:
                if i.get('a_p'):
                    self.sum += self.qs.filter(num_sc__cod_4=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.sum += self.qs.filter(num_sc__cod_4=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

    
    def get_sum_cod_5(self, ctx):
        if ctx.name.sum_cod_5:
            res = self.parse_cod_field(ctx.name.sum_cod_5)
            for i in res:
                if i.get('a_p'):
                    self.sum += self.qs.filter(num_sc__cod_5=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.sum += self.qs.filter(num_sc__cod_5=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

    def get_sum_bs1(self, ctx):
        if ctx.name.sum_bs1:
            res = self.parse_cod_field(ctx.name.sum_bs1)
            for i in res:
                if i.get('a_p'):
                    self.sum_bs += self.qs.filter(num_sc__bs1=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.sum_bs += self.qs.filter(num_sc__bs1=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()


    def get_min_cod_1(self, ctx):
        if ctx.name.min_cod_1:
            res = self.parse_cod_field(ctx.name.min_cod_1)
            for i in res:
                if i.get('a_p'):
                    self.min -= self.qs.filter(num_sc__cod_1=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.min -= self.qs.filter(num_sc__cod_1=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

    
    def get_min_cod_2(self, ctx):
        if ctx.name.min_cod_2:
            res = self.parse_cod_field(ctx.name.min_cod_2)
            for i in res:
                if i.get('a_p'):
                    self.min -= self.qs.filter(num_sc__cod_2=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.min -= self.qs.filter(num_sc__cod_2=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

        
    def get_min_cod_3(self, ctx):
        if ctx.name.min_cod_3:
            res = self.parse_cod_field(ctx.name.min_cod_3)
            for i in res:
                if i.get('a_p'):
                    self.min -= self.qs.filter(num_sc__cod_3=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.min -= self.qs.filter(num_sc__cod_3=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()


    def get_min_cod_4(self, ctx):
        if ctx.name.min_cod_4:
            res = self.parse_cod_field(ctx.name.min_cod_4)
            for i in res:
                if i.get('a_p'):
                    self.min -= self.qs.filter(num_sc__cod_4=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.min -= self.qs.filter(num_sc__cod_4=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()


    def get_min_cod_5(self, ctx):
        if ctx.name.min_cod_5:
            res = self.parse_cod_field(ctx.name.min_cod_5)
            for i in res:
                if i.get('a_p'):
                    self.min -= self.qs.filter(num_sc__cod_5=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.min -= self.qs.filter(num_sc__cod_5=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

    def get_min_bs1(self, ctx):
        if ctx.name.min_bs1:
            res = self.parse_cod_field(ctx.name.min_bs1)
            for i in res:
                if i.get('a_p'):
                    self.min_bs -= self.qs.filter(num_sc__bs1=i.get('code'), num_sc__a_p=i.get('a_p')).aggregate(s=Sum('iitg')).get('s') or 0
                else:
                    self.min_bs -= self.qs.filter(num_sc__bs1=i.get('code')).aggregate(s=Sum('iitg')).get('s') or 0
        return Success()

    def get_sum_vpr(self, ctx):
        if ctx.name.sum_vpr:
            res = self.parse_vpr(ctx.name.sum_vpr)
            for r in res:
                bs2_1 = self.qs.filter(num_sc__bs2=r.get('bs2_1')).aggregate(s=Sum('iitg')).get('s') or 0
                bs2_2 = self.qs.filter(num_sc__bs2=r.get('bs2_2')).aggregate(s=Sum('iitg')).get('s') or 0
                if r.get('sign') == '-':
                    self.sum_vpr += (bs2_1 - bs2_2) if (bs2_1 - bs2_2) > 0 else 0
                elif r.get('sign') == '+':
                    self.sum_vpr += (bs2_1 + bs2_2) if (bs2_1 + bs2_2) > 0 else 0
        return Success()

    
    def get_min_vpr(self, ctx):
        if ctx.name.min_vpr:
            res = self.parse_vpr(ctx.name.min_vpr)
            for r in res:
                bs2_1 = self.qs.filter(num_sc__bs2=r.get('bs2_1')).aggregate(s=Sum('iitg')).get('s') or 0
                bs2_2 = self.qs.filter(num_sc__bs2=r.get('bs2_2')).aggregate(s=Sum('iitg')).get('s') or 0
                if r.get('sign') == '-':
                    self.min_vpr -= (bs2_1 - bs2_2) if (bs2_1 - bs2_2) > 0 else 0
                elif r.get('sign') == '+':
                    self.min_vpr -= (bs2_1 + bs2_2) if (bs2_1 + bs2_2) > 0 else 0
        return Success()

    def show_result(self, ctx):
        bs1 = (self.sum_bs + self.min_bs) if (self.sum_bs + self.min_bs) < 0 else 0
        res = self.sum+self.min+self.sum_vpr+self.min_vpr
        if ctx.name.a_p == 'П':
            res -= bs1
        return Result(res)