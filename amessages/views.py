from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import AllowAny
from amessages.serializers import MessageSerializer
from amessages.models import AndroidMessage


class MessagesView(generics.ListCreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = MessageSerializer
    queryset = AndroidMessage.objects.all()
# Create your views here.
