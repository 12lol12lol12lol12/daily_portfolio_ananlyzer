from django.apps import AppConfig


class AmessagesConfig(AppConfig):
    name = 'amessages'
