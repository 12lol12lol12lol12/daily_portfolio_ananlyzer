from rest_framework import serializers

from amessages.models import AndroidMessage


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AndroidMessage
        fields = '__all__'