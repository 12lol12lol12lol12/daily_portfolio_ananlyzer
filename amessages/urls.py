from django.urls import path
from typing import List
from .views import MessagesView


app_name = 'amessages'

urlpatterns: List = [
    path('', MessagesView.as_view()),
]
