from __future__ import annotations
from stories import story, Success, Failure, arguments, Result
from dataclasses import dataclass
from typing import TYPE_CHECKING
from django.db import IntegrityError
from enum import Enum, auto

if TYPE_CHECKING:
    from user.repositories import UserRepo

@dataclass
class CreateUserStory:
    user_repo: UserRepo

    @story
    @arguments('username', 'email', 'password')
    def create(I):
        I.create_user
        I.send_email
        I.create_token
        I.show_result

    def create_user(self, ctx):
        user = self.user_repo.create_user(username=ctx.username, email=ctx.email, password=ctx.password)
        ctx.user = user
        return Success()

    def send_email(self, ctx):
        # TODO
        return Success()

    def create_token(self, ctx):
        self.user_repo.create_token(ctx.user)
        return Success()

    def show_result(self, ctx):
        return Result(ctx.user)
