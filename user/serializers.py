from rest_framework import serializers
from .models import User


class CreateUserSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(min_length=1, max_length=128)

    class Meta:
        model = User
        fields = ['username', 'password', 'password1', 'email']

    def validate(self, attrs):
        if len(attrs.get('password')) < 5:
            raise serializers.ValidationError('password length less than 5')
        if attrs.get('password') != attrs.get('password1'):
            raise serializers.ValidationError('Passwords do not match')
        return super().validate(attrs)


class ShowUserSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['username', 'email', 'token']

    def get_token(self, instance):
        return self.instance.auth_token.key