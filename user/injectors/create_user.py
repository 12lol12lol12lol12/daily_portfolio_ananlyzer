from dependencies import Injector, Package, this
from user.repositories import UserRepo
from user.services import CreateUserStory

class CreateUserInjector(Injector):
    story = CreateUserStory
    create = this.story.create
    user_repo = UserRepo
