from user.models import User
from django.db import IntegrityError
from typing import Union
from rest_framework.authtoken.models import Token

class UserRepo:

    @staticmethod
    def create_user(username: str, email: str, password: str) -> Union[User, IntegrityError]:
        return User.objects.create_user(username=username, email=email, password=password)

    @staticmethod
    def create_token(user: User) -> Token:
        return Token.objects.get_or_create(user=user)

    @staticmethod
    def get_token(user: User) -> str:
        return user.auth_token.key

