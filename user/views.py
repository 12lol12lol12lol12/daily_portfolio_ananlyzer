from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from .serializers import CreateUserSerializer, ShowUserSerializer
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from user.injectors import CreateUserInjector
from drf_yasg.utils import swagger_auto_schema
from user.models import User
from user.repositories import UserRepo
from parsefile.models import LoadedFile
import logging

logger = logging.getLogger('django')



class UserCreateAPIView(CreateAPIView):
    permission_classes = [AllowAny,]
    serializer_class = CreateUserSerializer

    def create(self, request, *args, **kwargs):
        """
        Create user with CreateUserStories
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        injector = CreateUserInjector
        result = injector.create.run(
            username=serializer.validated_data.get('username'),
            email=serializer.validated_data.get('email'),
            password=serializer.validated_data.get('password'))
        if result.is_success:
            return Response(ShowUserSerializer(result.value).data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class LoginUserView(APIView):
    """
    Возврат токена пользователю
    """
    permission_classes = [AllowAny,]

    def post(self, request):
        username, password = request.data.get('username'), request.data.get('password')

        if User.objects.filter(username=username).exists():
            user = User.objects.get(username=username)
        elif User.objects.filter(email=username).exists():
            user = User.objects.get(email=username)
        else:
            return Response({'username': ['User with this email or username does not exist']}, status=status.HTTP_400_BAD_REQUEST)

        if not user.check_password(password):
            return Response({'password': ['Incorrect password']}, status=status.HTTP_403_FORBIDDEN)
        
        print('return response')
        return Response({
            'username': user.username,
            'token': UserRepo.get_token(user),
            'email': user.email
            }, status=status.HTTP_200_OK)


class UserFilesView(APIView):
    """
    Возвращает список файлов пользователя
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        return Response(LoadedFile.objects.all().values('file'), status=status.HTTP_200_OK)

