from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import EmailField, BooleanField, CharField


class User(AbstractUser):
    is_active = BooleanField(default=True)
    email = EmailField(unique=True)

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self.__original_email = self.email
