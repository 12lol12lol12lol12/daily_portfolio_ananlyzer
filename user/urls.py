from django.urls import path
from .views import UserCreateAPIView, LoginUserView, UserFilesView
from typing import List


urlpatterns: List = [
    path('create_user', UserCreateAPIView.as_view()),
    path('login', LoginUserView.as_view()),
    path('files', UserFilesView.as_view()),
]
