defmodule User do
    @enforce_keys [:test]
    defstruct name: "John", age: 27, test: nil
end