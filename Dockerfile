# Official Python image
FROM registry.gitlab.com/12lol12lol12lol12/daily_portfolio_ananlyzer/app:base1
# create root directory for project, set the working directory and move requirements.txt file
COPY requirements.txt /home/backend
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ADD . /home/backend
# Web server will listen to this port
EXPOSE 8000
